package services.printers;

import repositories.models.Operation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class IPrinterImpl implements IPrinter {

    public void print(String accountId, List<Operation> operations) {
        System.out.println("Operations for the account " + accountId + " :");
        String lineDelimiter = "--------------------------------------------------------";
        System.out.println(lineDelimiter);
        if (operations.isEmpty()) {
            System.out.printf("%40s", "No operations were found");
            System.out.println();
        } else {
            System.out.printf("%10s %20s %20s", "DATE", "AMOUNT", "BALANCE");
            System.out.println();
            System.out.println(lineDelimiter);
            operations.forEach(this::printOperation);
        }
        System.out.println(lineDelimiter);
    }

    private void printOperation(Operation operation) {
        String prettyAmount = formatBigDecimal(operation.getAmount());
        String prettyBalance = formatBigDecimal(operation.getBalance());

        System.out.format("%10s %20s %20s", operation.getDate(), prettyAmount, prettyBalance);
        System.out.println();
    }

    private String formatBigDecimal(BigDecimal amount) {
        String sign = "";
        if (amount.compareTo(BigDecimal.ZERO) >= 0) {
            sign = "+";
        }
        return sign + amount.setScale(2, RoundingMode.HALF_DOWN);
    }
}
