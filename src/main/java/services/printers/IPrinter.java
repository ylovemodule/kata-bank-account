package services.printers;

import repositories.models.Operation;

import java.util.List;

public interface IPrinter {
    void print(String accountId, List<Operation> operations);
}
