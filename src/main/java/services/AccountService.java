package services;

import repositories.IAccountRepository;
import repositories.exceptions.AccountNotFoundException;
import repositories.models.Amount;
import repositories.models.Deposit;
import repositories.models.Operation;
import repositories.models.Withdrawal;
import services.printers.IPrinter;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;
import java.util.List;

public class AccountService {

    private final IAccountRepository repository;
    private final Clock clock;

    public AccountService(IAccountRepository repository, Clock clock) {
        this.repository = repository;
        this.clock = clock;
    }

    public Operation makeDeposit(Amount amount, String accountId) throws AccountNotFoundException {
        BigDecimal currentBalance = getAccountBalance(accountId);
        BigDecimal newBalance = currentBalance.add(amount.getValue());
        LocalDate currentDate = LocalDate.now(clock);
        Operation deposit = new Deposit(accountId, amount, newBalance, currentDate);
        return repository.save(deposit);
    }

    public Operation makeWithdrawal(Amount amount, String accountId) throws AccountNotFoundException {
        BigDecimal currentBalance = getAccountBalance(accountId);
        BigDecimal newBalance = currentBalance.subtract(amount.getValue());
        LocalDate currentDate = LocalDate.now(clock);
        Operation withdrawal = new Withdrawal(accountId, amount, newBalance, currentDate);
        return repository.save(withdrawal);
    }

    private BigDecimal getAccountBalance(String accountId) throws AccountNotFoundException {
        return repository.getBalance(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
    }

    public void printStatements(String id, IPrinter printer) throws AccountNotFoundException {
        List<Operation> operations = repository.getOperationsByAccountId(id).orElseThrow(() -> new AccountNotFoundException(id));
        printer.print(id, operations);
    }
}
