package repositories;

import repositories.models.Operation;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface IAccountRepository {
    Operation save(Operation operation);
    Optional<List<Operation>> getOperationsByAccountId(String id);
    Optional<BigDecimal> getBalance(String id);
}
