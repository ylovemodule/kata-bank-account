package repositories.models;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public abstract class Operation {
    private final LocalDate date;
    private final BigDecimal amount;
    private final BigDecimal balance;
    private final String accountId;

    Operation(String accountId, BigDecimal amount, BigDecimal balance, LocalDate date) {
        this.accountId = accountId;
        this.date = date;
        this.balance = balance;
        this.amount = amount;
    }

    public String getAccountId() {
        return accountId;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation = (Operation) o;
        return Objects.equals(date, operation.date) &&
                Objects.equals(amount, operation.amount) &&
                balance.compareTo(operation.balance)==0 &&
                Objects.equals(accountId, operation.accountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, amount, balance, accountId);
    }

    @Override
    public String toString() {
        return "Operation{" +
                "date=" + date +
                ", amount=" + amount +
                ", balance=" + balance +
                ", accountId='" + accountId + '\'' +
                '}';
    }
}
