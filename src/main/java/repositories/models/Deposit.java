package repositories.models;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Deposit extends Operation {

    public Deposit(String accountId, Amount amount, BigDecimal balance, LocalDate date) {
        super(accountId, amount.getValue(), balance, date);
    }
}
