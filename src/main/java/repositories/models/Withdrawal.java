package repositories.models;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Withdrawal extends Operation{

    public Withdrawal(String accountId, Amount amount, BigDecimal balance, LocalDate date) {
        super(accountId, amount.getValue().negate(), balance, date);
    }
}
