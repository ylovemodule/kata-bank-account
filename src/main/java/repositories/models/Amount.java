package repositories.models;

import java.math.BigDecimal;
import java.util.Optional;

public class Amount {
    private final BigDecimal value;

    private Amount(BigDecimal value) {
        this.value = value;
    }

    public static Optional<Amount> of(BigDecimal value){
        if(BigDecimal.ZERO.compareTo(value) >= 0){
            return Optional.empty();
        }
        return Optional.of(new Amount(value));
    }

    public BigDecimal getValue() {
        return value;
    }
}
