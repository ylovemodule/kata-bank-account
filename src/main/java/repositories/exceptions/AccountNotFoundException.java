package repositories.exceptions;

public class AccountNotFoundException extends Exception {
    public AccountNotFoundException(String idAccount) {
        super("No account found for id " + idAccount);
    }
}
