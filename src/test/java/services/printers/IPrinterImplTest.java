package services.printers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import repositories.models.Amount;
import repositories.models.Deposit;
import repositories.models.Operation;
import repositories.models.Withdrawal;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IPrinterImplTest {

    private ByteArrayOutputStream consoleOutputStream;
    private PrintStream formerOutputPrintStream;
    private static final String ACCOUNT_ID = "45123";

    @BeforeEach
    void redirectOutputStream() {
        consoleOutputStream = new ByteArrayOutputStream();
        formerOutputPrintStream = System.out;
        System.setOut(new PrintStream(consoleOutputStream));
    }

    @AfterEach
    void restoreOutputStream() {
        System.setOut(formerOutputPrintStream);
    }

    @Test
    @DisplayName("Prints a formatted list of operations")
    void it_should_print_a_formatted_list_of_operations() {
        String expected =
                "Operations for the account 45123 :"                            + System.lineSeparator() +
                "--------------------------------------------------------"      + System.lineSeparator() +
                "      DATE               AMOUNT              BALANCE"          + System.lineSeparator() +
                "--------------------------------------------------------"      + System.lineSeparator() +
                "2019-04-02               -37.00              +113.00"          + System.lineSeparator() +
                "2019-01-01              +150.00              +150.00"          + System.lineSeparator() +
                "--------------------------------------------------------"      + System.lineSeparator();

        Amount amountDeposit = Amount.of(BigDecimal.valueOf(150.003)).get();
        Amount amountWithdrawal = Amount.of(BigDecimal.valueOf(37)).get();

        LocalDate dateWithdrawal = LocalDate.of(2019, 4, 2);
        LocalDate dateDeposit = LocalDate.of(2019, 1, 1);

        Operation withdrawal = new Withdrawal(ACCOUNT_ID, amountWithdrawal, BigDecimal.valueOf(113), dateWithdrawal);
        Operation deposit = new Deposit(ACCOUNT_ID, amountDeposit, amountDeposit.getValue(), dateDeposit);

        IPrinterImpl printer = new IPrinterImpl();
        printer.print(ACCOUNT_ID, List.of(withdrawal, deposit));
        assertEquals(expected, consoleOutputStream.toString());
    }

    @Test
    @DisplayName("Prints a message explaining there are no operations")
    void it_should_print_a_message_when_the_list_of_operations_is_empty() {
        String expected =
                "Operations for the account 45123 :"                            + System.lineSeparator() +
                "--------------------------------------------------------"      + System.lineSeparator() +
                "                No operations were found"                      + System.lineSeparator() +
                "--------------------------------------------------------"      + System.lineSeparator();

        IPrinterImpl printer = new IPrinterImpl();
        printer.print(ACCOUNT_ID, List.of());
        assertEquals(expected, consoleOutputStream.toString());
    }
}