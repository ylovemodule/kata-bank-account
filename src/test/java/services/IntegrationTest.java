package services;

import org.junit.jupiter.api.*;
import repositories.FakeAccountRepository;
import repositories.exceptions.AccountNotFoundException;
import repositories.models.Amount;
import repositories.models.Operation;
import services.printers.IPrinterImpl;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class IntegrationTest {
    private static final String ID1 = "1";
    private static final String ID2 = "2";
    private static final String ID3 = "3";

    private static AccountService accountService;
    private final IPrinterImpl printer = new IPrinterImpl();
    private static Clock fixedClock = Clock.fixed(Instant.parse("2019-01-01T10:15:30Z"), ZoneId.of("UTC"));

    private ByteArrayOutputStream consoleOutputStream;
    private PrintStream formerOutputPrintStream;

    @BeforeEach
    void redirectOutputStream() {
        consoleOutputStream = new ByteArrayOutputStream();
        formerOutputPrintStream = System.out;
        System.setOut(new PrintStream(consoleOutputStream));
    }

    @BeforeAll
    static void setUp(){
        HashMap<String, LinkedList<Operation>> operations = new HashMap<>();
        operations.put(ID1, new LinkedList<>());
        operations.put(ID2, new LinkedList<>());
        FakeAccountRepository operationRepository = new FakeAccountRepository(operations);
        accountService = new AccountService(operationRepository, fixedClock);
    }

    @AfterEach
    void restoreOutputStream() {
        System.setOut(formerOutputPrintStream);
    }

    @Test
    @DisplayName("Make a deposit and a withdrawal on an account and print all statements")
    void it_should_make_1_deposit_and_1_withdrawal_then_get_operations_for_account_2() throws AccountNotFoundException {
        Amount amountDeposit = Amount.of(BigDecimal.valueOf(150)).get();
        Amount amountWithdrawal = Amount.of(BigDecimal.valueOf(37)).get();

        accountService.makeDeposit(amountDeposit, ID1);
        accountService.makeWithdrawal(amountWithdrawal, ID1);
        accountService.printStatements(ID1, printer);

        String expected =
                "Operations for the account 1 :"                            + System.lineSeparator() +
                        "--------------------------------------------------------"      + System.lineSeparator() +
                        "      DATE               AMOUNT              BALANCE"          + System.lineSeparator() +
                        "--------------------------------------------------------"      + System.lineSeparator() +
                        "2019-01-01               -37.00              +113.00"          + System.lineSeparator() +
                        "2019-01-01              +150.00              +150.00"          + System.lineSeparator() +
                        "--------------------------------------------------------"      + System.lineSeparator();

        assertEquals(expected, consoleOutputStream.toString());
    }

    @Test
    @DisplayName("Throws an exception if the account is not found")
    void it_should_throw_an_exception_when_i_try_to_make_an_operation_on_an_unknown_account(){
        assertThrows(AccountNotFoundException.class, () -> accountService.makeDeposit(Amount.of(BigDecimal.TEN).get(), ID3));
        assertThrows(AccountNotFoundException.class, () -> accountService.makeWithdrawal(Amount.of(BigDecimal.TEN).get(), ID3));
        assertThrows(AccountNotFoundException.class, () -> accountService.printStatements(ID3, printer));
    }
}
