package services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import repositories.IAccountRepository;
import repositories.exceptions.AccountNotFoundException;
import repositories.models.Amount;
import repositories.models.Deposit;
import repositories.models.Operation;
import repositories.models.Withdrawal;
import services.printers.IPrinter;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class AccountServiceTest {

    @InjectMocks
    private AccountService service;

    @Mock
    private IAccountRepository repository;

    @Mock
    private IPrinter printer;

    @Mock
    private Clock clock;

    private static final String ACCOUNT_ID = "accountIdentifier";

    private final LocalDate expectedDate = LocalDate.of(2019,01,01);

    @BeforeEach
    void setUp() {
        initMocks(this);
        String instantExpected = "2019-01-01T10:15:30Z";
        when(clock.instant()).thenReturn(Instant.parse(instantExpected));
        when(clock.getZone()).thenReturn(ZoneId.of("UTC"));
    }

    @Test
    @DisplayName("Creates a deposit when i save money")
    void it_create_a_deposit_when_i_save_money() throws Exception {
        BigDecimal value = BigDecimal.valueOf(37);
        Amount amount = Amount.of(value).orElseThrow(Exception::new);

        Operation expectedOperation = new Deposit(ACCOUNT_ID, amount, value, expectedDate);
        when(repository.getBalance(ACCOUNT_ID)).thenReturn(Optional.of(BigDecimal.ZERO));
        when(repository.save(expectedOperation)).thenReturn(expectedOperation);

        assertEquals(expectedOperation, service.makeDeposit(amount, ACCOUNT_ID));

        verify(repository, times(1)).getBalance(ACCOUNT_ID);
        verify(repository, times(1)).save(expectedOperation);
        verifyNoMoreInteractions(repository);
    }

    @Test
    @DisplayName("Throws an exception when i save money on an unknown account")
    void it_should_throw_an_exception_if_i_try_to_make_a_deposit_and_the_account_is_not_found() throws Exception {
        BigDecimal value = BigDecimal.valueOf(37);
        Amount amount = Amount.of(value).orElseThrow(Exception::new);

        when(repository.getBalance(ACCOUNT_ID)).thenReturn(Optional.empty());

        assertThrows(AccountNotFoundException.class, () -> service.makeDeposit(amount, ACCOUNT_ID));

        verify(repository, times(1)).getBalance(ACCOUNT_ID);
        verifyNoMoreInteractions(repository);
    }

    @Test
    @DisplayName("Creates a withdrawal when i withdraw money")
    void it_create_a_withdrawal_when_i_withdraw_money() throws Exception {
        BigDecimal value = BigDecimal.valueOf(37);
        Amount amount = Amount.of(value).orElseThrow(Exception::new);

        Operation expectedOperation = new Withdrawal(ACCOUNT_ID, amount, value.negate(), expectedDate);
        when(repository.getBalance(ACCOUNT_ID)).thenReturn(Optional.of(BigDecimal.ZERO));
        when(repository.save(expectedOperation)).thenReturn(expectedOperation);

        assertEquals(expectedOperation, service.makeWithdrawal(amount, ACCOUNT_ID));

        verify(repository, times(1)).getBalance(ACCOUNT_ID);
        verify(repository, times(1)).save(expectedOperation);
        verifyNoMoreInteractions(repository);
    }

    @Test
    @DisplayName("Throws an exception when i withdraw money from an unknown account")
    void it_should_throw_an_exception_if_i_try_to_make_a_withdrawal_and_the_account_is_not_found() throws Exception {
        BigDecimal value = BigDecimal.valueOf(37);
        Amount amount = Amount.of(value).orElseThrow(Exception::new);

        when(repository.getBalance(ACCOUNT_ID)).thenReturn(Optional.empty());

        assertThrows(AccountNotFoundException.class, () -> service.makeWithdrawal(amount, ACCOUNT_ID));

        verify(repository, times(1)).getBalance(ACCOUNT_ID);
        verifyNoMoreInteractions(repository);
    }

    @Test
    @DisplayName("Print a list of operations for the given account")
    void it_should_return_the_list_of_operations_for_given_account() throws AccountNotFoundException {
        Amount amountDeposit = Amount.of(BigDecimal.valueOf(150)).get();
        Amount amountWithdrawal = Amount.of(BigDecimal.valueOf(37)).get();
        Operation deposit = new Deposit(ACCOUNT_ID, amountDeposit, amountDeposit.getValue(), expectedDate);
        Operation withdrawal = new Withdrawal(ACCOUNT_ID, amountWithdrawal, amountWithdrawal.getValue(), expectedDate);
        List<Operation> operations = List.of(deposit, withdrawal);

        when(repository.getOperationsByAccountId(ACCOUNT_ID)).thenReturn(Optional.of(operations));
        doNothing().when(printer).print(ACCOUNT_ID, operations);

        service.printStatements(ACCOUNT_ID, printer);

        verify(repository, times(1)).getOperationsByAccountId(ACCOUNT_ID);
        verify(printer, times(1)).print(ACCOUNT_ID, operations);
        verifyNoMoreInteractions(repository);
        verifyNoMoreInteractions(printer);
    }
}