package repositories.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AmountTest {

    @Test
    @DisplayName("Creates an amount when the given value is positive")
    void it_should_create_an_amount_with_a_value_equal_to_given_argument_if_value_is_is_positive() {
        BigDecimal value = BigDecimal.TEN;
        Optional<Amount> amount = Amount.of(value);
        assertEquals(0, value.compareTo(amount.get().getValue()));
    }

    @Test
    @DisplayName("Returns an empty amount when the given value is strictly negative")
    void it_should_return_an_empty_optional_when_i_try_to_create_a_negative_amount(){
        assertEquals(Optional.empty(), Amount.of(BigDecimal.TEN.negate()));
    }
}