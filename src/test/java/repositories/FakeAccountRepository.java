package repositories;

import repositories.models.Operation;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class FakeAccountRepository implements IAccountRepository {

    private final Map<String, LinkedList<Operation>> operations;

    public FakeAccountRepository(Map<String, LinkedList<Operation>> operations) {
        this.operations = operations;
    }

    @Override
    public Operation save(Operation operation) {
        if(operations.containsKey(operation.getAccountId())){
            operations.get(operation.getAccountId()).push(operation);
            return operation;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Optional<List<Operation>> getOperationsByAccountId(String id) {
        if(operations.containsKey(id)){
            return Optional.of(operations.get(id));
        }
        return Optional.empty();
    }

    @Override
    public Optional<BigDecimal> getBalance(String id) {
        if(operations.containsKey(id)){
            LinkedList<Operation> accountOperations = this.operations.get(id);
            if(accountOperations.size()==0){
                return Optional.of(BigDecimal.ZERO);
            }
            return Optional.of(accountOperations.getFirst().getBalance());
        }
        return Optional.empty();
    }
}
